+++
title = "About"
author = ["binarydigitz01"]
lastmod = 2023-04-13T23:12:09+05:30
draft = false
[menu]
  [menu.main]
    weight = 1002
    identifier = "about"
+++

## Hi There {#hi-there}

Welcome to my blog! I'm Arnav Vijaywargiya, alias binarydigitz01. I have created this blog to post about software that I use, which is mostly about [Emacs](https://www.gnu.org/software/emacs/) and [Nixos](https://nixos.org).

Contact info:

-   Mail: binarydigitz01 at protonmail dot com.
-   [Mastadon: binarydigitz01@emacs.ch](https://emacs.ch/@binarydigitz01)
-   [Gitlab](https://www.gitlab.com/binarydigitz01)
