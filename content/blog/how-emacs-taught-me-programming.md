+++
title = "How Emacs taught me programming"
author = ["binarydigitz01"]
date = 2024-09-15T16:14:00+05:30
lastmod = 2024-10-27T12:29:08+05:30
tags = ["emacs"]
draft = false
+++

I've been using emacs for around 4 years now, and as I'm entering the 2nd year of my college, I can
feel I've grown significanly along with my text editor.

Emacs (and Linux) has taught me a lot about Software Engineering. Reading other people's
configuration files, writing my own unique snippets, and playing with packages has really helped me
learn programming. Many programmers advice to make a "project" to really get a grasp of programming
and get experience, for me a large part of that was emacs. While I was using emacs to code, I was
also learning how to read the manual, refer the web to figure out problems, and gather help from
forums(You guys rock).

I started my journey with doom emacs, then made my own config, which with inspiration from prot's
dotfiles, is now modular. After reading a little bit of practical common lisp, I feel a lot more
productive emacs. I also tried common lisp, and loved it. I kind of dislike how it ruined java and
python for me, and now I have to explain quite a bit when people say what my favourite programming
language is.

The biggest thing that Emacs and linux taught me, however, was to figure out things for
myself. Instead of relying on others, I learnt to read the manual (Yes arch users, I actually read
the manual).
Thanks Emacs Community!
