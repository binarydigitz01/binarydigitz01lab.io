+++
title = "Using spritely hoot on nixos"
author = ["binarydigitz01"]
lastmod = 2025-02-02T19:34:17+05:30
tags = ["guile"]
draft = false
+++

Recently Spritely institute (<https://spritely.institute>), released a new update for hoot and goblins, together! I have been looking at their work for quite some time now, and figured this is the best time to get into their awesome work.
For those who aren't aware, hoot is a scheme to web assembly (wasm) compiler implemented in guile, so that you could write guile on the web. I was extremely excited as i hate javascript with a burning passion, but unfortunatley you still need wrapper javascript code to actually load the assembly :(


## How to get hoot on nixos? {#how-to-get-hoot-on-nixos}

This was my first major hurdle. Hoot documentation tells you how to get it with the guix package manager, but i would rather not have both guix and nix on my machine. The 2nd option was to build guile from the master branch, for which there was no existing guile-git package. However i found an overlay from r/nixos subbreddit, which is exactly what i needed:

```nix
  {
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };
  outputs = { self, nixpkgs, ... }@inputs :
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ self.overlays.default ];
      };
    in
      {
        overlays.default = final: prev: {
          guile_3_0 = prev.guile_3_0.overrideAttrs (old: {
            version = "main";
            src = (prev.fetchFromSavannah {
              repo = "guile";
              rev = "5d3f561d7dbcee370dc764cd5ba4210c62ce13de";
              hash = "sha256-i6k0PZSLT/y/fvyY1mGUQIkToy93GX0qm/H55FzOS8w=";
            });
            # Fails to apply the second patch so skipping that one for now
            # patches = (prev.lib.lists.take 1 old.patches) ++ (prev.lib.lists.drop 2 old.patches);
            nativeBuildInputs = old.nativeBuildInputs ++ [
              prev.autoconf
              prev.automake
              prev.libtool
              prev.flex
              prev.gettext
              prev.texinfo
              prev.gperf
            ];

            preConfigure = ''
            set -ex

            autoreconf --verbose --install --force
            '';
          });
        };
        devShells.x86_64-linux.default = pkgs.mkShellNoCC {
          buildInputs = with pkgs; [guile guile-hoot];
        };
      };
}
```


## Getting started with hoot {#getting-started-with-hoot}

I was following hoot's blog posts, namely <https://spritely.institute/news/building-interactive-web-pages-with-guile-hoot.html>.
It's a really good introduction to hoot's ffi, something that is essential to interact with the DOM. However the code on the post is slightly outdated, and i didn't figure it out until 5 hours later. I wish they'd mark the post outdated or something, but the api is marked unstable so i don't blame them. They have a collection of demos at <https://gitlab.com/spritely/guile-hoot-ffi-demo>, that really helped me out in figuring what i was doing wrong. In short, instead of:

```javascript
  window.addEventListener("load", function() {
  Scheme.load_main("hello.wasm", {}, {
    document: {
      body() { return document.body; },
      createTextNode: Document.prototype.createTextNode.bind(document)
    },
    element: {
      appendChild(parent, child) { return parent.appendChild(child); }
    }
  });
});
```

it should be something like this:

```javascript
  window.addEventListener("load", async function() {
  Scheme.load_main("hello.wasm", {
    user_imports: {
      document: {
        body() { return document.body; },
        createTextNode: Document.prototype.createTextNode.bind(document)
      },
      element: {
        appendChild(parent, child) { return parent.appendChild(child); }
      }
    }
  });
});
```

Anyways i managed to make a basic hello world app, I'll try to play with it more, as I would definitely like to use in for my college projects.
