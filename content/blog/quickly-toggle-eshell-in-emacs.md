+++
title = "Quickly toggle Eshell in emacs"
author = ["binarydigitz01"]
date = 2023-04-13T22:27:00+05:30
lastmod = 2023-04-13T23:12:44+05:30
tags = ["emacs"]
draft = false
+++

One of my Emacs' favourite features is Eshell. However recently I wanted to create a function, that
automatically toggles eshell, like vscode, and gives priority to the project root (if in project).
This little code snippet does exactly that:

```emacs-lisp
(defun binary-eshell/toggle-eshell ()
  (interactive)
  (let ((eshell-buffer-name (binary-eshell/eshell-buffer-name)))
    (if (binary-eshell/eshell-toggled-p)
        (delete-windows-on eshell-buffer-name)
      (progn
        (split-window-below)
        (if (project-current)
            (let ((default-directory (project-root (project-current))))
              (eshell)))
        (eshell)))))

(defun binary-eshell/eshell-toggled-p ()
  "Checks if eshell is toggled."
  (let ((eshell-buffer-name (binary-eshell/eshell-buffer-name))
        (result))
    (dolist (element (window-list) result)
      (if (string= eshell-buffer-name (buffer-name (window-buffer element)))
          (setq result t)))
    result))

(defun binary-eshell/eshell-buffer-name ()
  "Returns the name of the eshell buffer. It works on the basis of the following rule:
If the current buffer is part of a project, then name it on the basis of the project,
else name it on the basis of default-directory."
  (let ((eshell-buffer-name))
    (if (project-current)
        (setq eshell-buffer-name
              (concat "*eshell-" (project-name (project-current)) "*"))
      (setq eshell-buffer-name (concat "*eshell-" default-directory "*")))))
```

Call binary-eshell/toggle-eshell and voila! It opens an eshell window, for you to work on!
With this, I have hopefully shown you how you can incorporate eshell even more into your workflow.
