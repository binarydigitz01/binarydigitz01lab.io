+++
title = "Ricing Org Mode"
author = ["binarydigitz01"]
date = 2023-04-23T23:24:00+05:30
lastmod = 2023-04-23T23:36:06+05:30
tags = ["emacs", "org-mode"]
draft = false
+++

A majority of emacs users are absolute die-hard fans of org mode, and wouldn't be able to live without it. And it makes sense too, it's a tool that you can use in whatever workflow you prefer, which is really powerful. However as a relatively new emacs users (only 2 years!), I couldn't really get org mode. I never felt satisfied when using it, as for me it seemed like an old technology, especially for a person who's used to the web apps, which have a totally different UI .

However, I recently spent some time to prettify org mode, and I must say, it makes _all the difference_ in the world! Here's a comparision of my old and new config:

![](/ox-hugo/Initial_Config.png)![](/ox-hugo/Final_config.png)

As you can see, the second one looks much better, and more comparable to modern note taking apps. As for me, it makes all the difference in the world. And it took me only 30 minutes to make these changes!


## The changes {#the-changes}


### Centred text {#centred-text}

I have used [olivetti mode](https://github.com/rnkn/olivetti) to centre the text. This makes a huge impact on writing, as almost all prose and text you read are centred. For me this was easily the one with the most impact. All you have to do is add a hook:

```nil
(use-package olivetti
:hook (org-mode . olivetti-mode))
```

Make sure to turn off line numbers mode if you're using it, as that makes olivtti mode pretty ugly in my opinion, plus it's absolutely useless for org mode.


### Font {#font}

Believe it or not, changing to a variable pitch font also had a pretty significant impact. At first I did not think this would make much of an impact, as I thought I was used to monospaced font as a programmer, but turns out that was a lie. If you use a good variable pitch face, it looks really good. Right now i'm using EtBook font, which actually looks really nice! Here's the relevant font config:

```emacs-lisp
;; This part has been copied and slightly modified from
;; https://zzamboni.org/post/beautifying-org-mode-in-emacs/

(let* ((variable-tuple
        (cond ((x-list-fonts "ETBembo") '(:font "ETBembo"))))
       (headline           `(:weight bold)))

  (custom-theme-set-faces
   'user
   `(org-level-8 ((t (,@headline ,@variable-tuple))))
   `(org-level-7 ((t (,@headline ,@variable-tuple))))
   `(org-level-6 ((t (,@headline ,@variable-tuple))))
   `(org-level-5 ((t (,@headline ,@variable-tuple))))
   `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
   `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
   `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
   `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
   `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))))
```

The cond statement checks whether ETBombo is installed on my system. The relevant portion for variable pitch mode:

```emacs-lisp
(custom-theme-set-faces
 'user
 '(variable-pitch ((t (:family "ETBembo" :height 170 :weight thin))))
 '(fixed-pitch ((t ( :family "Fira Code Nerd Font" :height 130)))))
```

Now the problem with variable pitch mode is that it makes all text variable pitched, which includes tables and code blocks, which is something you absolutely do not want. To fix that, include this code in your configuration:

```emacs-lisp
  ;; Copied from stackoverflow, this retains colors for org src blocks and tables, while making them monospaced
(defun my-adjoin-to-list-or-symbol (element list-or-symbol)
  (let ((list (if (not (listp list-or-symbol))
                  (list list-or-symbol)
                list-or-symbol)))
    (require 'cl-lib)
    (cl-adjoin element list)))

(eval-after-load "org"
  '(mapc
    (lambda (face)
      (set-face-attribute
       face nil
       :inherit
       (my-adjoin-to-list-or-symbol
        'fixed-pitch
        (face-attribute face :inherit))))
    (list 'org-code 'org-block 'org-table)))
```

The above code has been copied from [here](https://stackoverflow.com/questions/3758139/variable-pitch-for-org-mode-fixed-pitch-for-tables).

The colors for headings are provided by my theme, which is ef-summer from ef-themes by Protesilaos.


### Conclusion {#conclusion}

As you can see, configuring org mode is quite a hassle, especially setting up faces. My config is almost entirely copy pasted from other users, but hey, what can you expect from a 2 year user? The point of this post was to quickly help a person get started with org mode, and to demonstrate some common hurdles that any new emacs user will probably have if they use vanilla emacs.
