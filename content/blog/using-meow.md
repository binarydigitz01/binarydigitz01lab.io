+++
title = "Using meow.el for modal editing"
author = ["binarydigitz01"]
lastmod = 2023-07-29T00:02:15+05:30
draft = false
+++

I have been using meow.el instead of evil for my modal editing since the past 4 months, and have mixed reviews about it. Read on if you would like to decide to try meow for yourself.


## Introduction {#introduction}

Meow mode takes insipiration from kakoune style keybindings. In vim's modal bindings, you have to type the verb first (deletion, pasting, etc.) then select the portion on which to act (word, paragraph, etc.). This method is error prone, not only for beginners but for experienced users as well.

Kakoune introduced a new concept: What if instead of first typing the verb, we type the motion? This is what meow does as well, it allows you to first select the region, then use the verb. One of the advantages of this method is that it allows you to visually see the text on which you want to act on.


## Meow's excellent quality design {#meow-s-excellent-quality-design}

Meow has been designed to be really compatible with emacs, unlike evil. It's Beacon mode and Keypad state are 2 killer integral features which I really like. Although I must admit I haven't really used beacon mode all that much.

If you guys don't know what beacon mode does, it basically integrates kmacros in a "meow" way, which is super convenient.

Keypad state is the equivalent of the leader key in evil, but the twist here is that you can use all binded keys from keypad state, which basically makes it god mode.
As I have never used god mode, for me keypad state was a god sent tool, especially since I won't have to bind a lot of the default bindings to new keys.

By the way, both of these features have been extensively documented in the meow tutorial, so I haven't explained them to their fullest capabilities.

meow-visit is another utility feature that's really convenient for jumping across the buffer. Here's meow-visit in practice:

{{< figure src="/ox-hugo/ss1.png" >}}

Yeah that's right, it's basically isearch but with autocomplete, really handy for me.

These are all the main features that I frequently use out of meow, and have made a huge impact on my workflow.


## Minimum config {#minimum-config}

```emacs-lisp
(use-package meow
  :init
  ;; This allows 'a' key to work as in vim.
  (setq meow-use-cursor-position-hack t))
(require 'meow)

(defun meow-setup ()
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   ;; SPC j/k will run the original command in MOTION state.
   '("j" . "H-j")
   '("k" . "H-k")
   ;; Use SPC (0-9) for digit arguments.
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . meow-beginning-of-thing)
   '("]" . meow-end-of-thing)
   '("a" . meow-append)
   '("A" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("d" . meow-delete)
   '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . meow-next-symbol)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '("I" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . meow-join)
   '("n" . meow-search)
   '("o" . meow-block)
   '("O" . meow-to-block)
   '("p" . meow-yank)
   '("q" . meow-quit)
   '("Q" . meow-clipboard-save)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-visit)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . meow-goto-line)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '("<escape>" . ignore)))
```

It's a really lenghty config for a minimum configuration, but I believe that is for the best, as it allows you to really customize everything to your needs. Meow really is just an ideal, you can configure the bindings exactly to your needs!
