+++
title = "My Workflow"
author = ["binarydigitz01"]
lastmod = 2023-04-12T23:11:14+05:30
draft = false
+++

I use Emacs as my main text editor of choice, mainly because I'm trying to incorporate org mode
into my workflow.I use the [Doom Emacs](https:www.github.com/doomemacs/doomemacs) Configuration, as it is basically what I want, with a very
speedy code base. My configuration is not a literate one unfortunately, but it is available at my
[dotfiles repo](https:www.gitlab.com/binarydigitz01/dotfiles).

The other major tool that i use is [Nixos](https:www.nixos.org). It's an operating system based on the functional package
manager Nix, which is an absolute game changer in writing software. More importantly, it means you
can have different versions of the same package for the different software that you may be
developing on your machine. Also, it makes it easy for developers to make sure that the software
works on everyone's machine. It's not for everyone though, and it needs serious dedication to get
into the nix ecosystem, but if you do, boy oh boy does it pay off.

So when I start a new project, I firstly create/initialize a flake template. Then I use emacs to
create an Org Roam node for that corresponding project, which contains all the important information
that I need, in one place for that project. Since I am a student, this mostly contains Documentation
links, and important blog posts which contain helpful links.

This is the majority of my workflow, which will hopefully grow into a more complex one as I learn
more about emacs and nix!
