+++
title = "Optional dependency loading in Emacs, a good idea?"
author = ["binarydigitz01"]
date = 2024-10-27T12:12:00+05:30
lastmod = 2024-10-27T12:28:34+05:30
tags = ["emacs", "nixos"]
draft = false
+++

Hey guys!
I  have gotten a sudden motivation to refactor my nixos dotfiles, and I have an idea, don't know if it's good or not so here it is:

While developing code, I feel like your system should have 0 development dependencies, all of them should be project dependencies instead. However my text editor has certain configuration files for all languages, so it has packages installed for all of the possible languages and frameworks and tools that i'll be using.

Instead of that, would it be a wise decision so that emacs only loads code and installs packages related to the project I'm currently working on?
I'd love to hear your opinions! Reach out to me at Fosstodon: @binarydigitz01@fosstodon.org, or mail
me at binarydigitz01 at protonmail.com.
